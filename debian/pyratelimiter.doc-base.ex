Document: pyratelimiter
Title: Debian pyratelimiter Manual
Author: <insert document author here>
Abstract: This manual describes what pyratelimiter is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/pyratelimiter/pyratelimiter.sgml.gz

Format: postscript
Files: /usr/share/doc/pyratelimiter/pyratelimiter.ps.gz

Format: text
Files: /usr/share/doc/pyratelimiter/pyratelimiter.text.gz

Format: HTML
Index: /usr/share/doc/pyratelimiter/html/index.html
Files: /usr/share/doc/pyratelimiter/html/*.html
