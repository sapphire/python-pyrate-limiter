#
# Regular cron jobs for the pyratelimiter package.
#
0 4	* * *	root	[ -x /usr/bin/pyratelimiter_maintenance ] && /usr/bin/pyratelimiter_maintenance
