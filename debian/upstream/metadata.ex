# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/pyratelimiter/issues
# Bug-Submit: https://github.com/<user>/pyratelimiter/issues/new
# Changelog: https://github.com/<user>/pyratelimiter/blob/master/CHANGES
# Documentation: https://github.com/<user>/pyratelimiter/wiki
# Repository-Browse: https://github.com/<user>/pyratelimiter
# Repository: https://github.com/<user>/pyratelimiter.git
